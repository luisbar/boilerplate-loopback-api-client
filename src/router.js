/********************
 * From third party *
 ********************/
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
/****************
 * From project *
 ****************/
import BasisComponent from 'basisComponent';
import { NotFound, Login, Home, RegisterPersonForm, PersonViewer, UpdatePersonForm, Roles, RegisterRoleForm, RolesAllocation, RoleViewer, UpdateRoleForm } from 'view';
import { LOGIN, HOME, REGISTER_PERSON_FORM, PERSON_VIEWER, UPDATE_PERSON_FORM, ROLES, REGISTER_ROLE_FORM, ROLE_VIEWER, UPDATE_ROLE_FORM, ROLES_ALLOCATION } from 'config';
import PrivateRoute from 'privateRoute';
/**
 * It has all routes and manages them
 */
class Router extends BasisComponent {

  constructor(props) {
    super(props);

    this._renderUpdateRoleForm = this.renderUpdateRoleForm.bind(this);
    this._renderRoleViewer = this.renderRoleViewer.bind(this);
    this._renderRegisterRoleForm = this.renderRegisterRoleForm.bind(this);
    this._renderUpdatePersonForm = this.renderUpdatePersonForm.bind(this);
    this._renderRolesAllocation = this.renderRolesAllocation.bind(this);
    this._renderRoles = this.renderRoles.bind(this);
    this._renderPersonViewer = this.renderPersonViewer.bind(this);
    this._renderRegisterPersonForm = this.renderRegisterPersonForm.bind(this);
    this._renderHome = this.renderHome.bind(this);
    this._renderLogin = this.renderLogin.bind(this);
    this._renderNotFound = this.renderNotFound.bind(this);
  }

  render() {

    return (
      this.props.checked &&
      <Switch location={this.props.location}>
        <PrivateRoute exact path={ROLES_ALLOCATION} component={this._renderRolesAllocation} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={UPDATE_ROLE_FORM} component={this._renderUpdateRoleForm} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={REGISTER_ROLE_FORM} component={this._renderRegisterRoleForm} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={ROLE_VIEWER} component={this._renderRoleViewer} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={ROLES} component={this._renderRoles} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={UPDATE_PERSON_FORM} component={this._renderUpdatePersonForm} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={REGISTER_PERSON_FORM} component={this._renderRegisterPersonForm} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={PERSON_VIEWER} component={this._renderPersonViewer} authenticated={this.props.authenticated}/>
        <PrivateRoute exact path={HOME} component={this._renderHome} authenticated={this.props.authenticated}/>
        <Route exact path={LOGIN} render={this._renderLogin}/>
        <Route render={this._renderNotFound}/>
      </Switch>
    );
  }

  renderUpdateRoleForm() {

    return (
      <UpdateRoleForm/>
    );
  }

  renderRoleViewer() {

    return (
      <RoleViewer/>
    );
  }

  renderRegisterRoleForm() {

    return (
      <RegisterRoleForm/>
    );
  }

  renderUpdatePersonForm() {

    return (
      <UpdatePersonForm/>
    );
  }

  renderRolesAllocation() {

    return (
      <RolesAllocation/>
    );
  }

  renderRoles() {

    return (
      <Roles/>
    );
  }

  renderPersonViewer() {

    return (
      <PersonViewer/>
    );
  }

  renderRegisterPersonForm() {

    return (
      <RegisterPersonForm/>
    );
  }

  renderHome() {

    return (
      <Home/>
    );
  }

  renderLogin() {

    return (
      <Login/>
    );
  }

  renderNotFound() {

    return (
      <NotFound/>
    );
  }
}

const mapStateToProps = (state) => ({
  checked: state
  .rootReducer
  .sessionReducer
  .checked,
  authenticated: state
  .rootReducer
  .sessionReducer
  .authenticated,
});

export default withRouter(connect(
  mapStateToProps,
)(Router));
