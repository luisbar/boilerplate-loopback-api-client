//ErrorBoundary
const TXT_1 = 'Hubo un problema, lo sentimos si tienes algún inconveniente con la página.';
const TXT_2 = 'Detalle';

//NotFound
const TXT_3 = 'No se encontro el recurso :(';

//Login
const TXT_4 = 'Login';
const TXT_5 = 'Usuario';
const TXT_6 = 'Contraseña';
const TXT_7 = 'Entrar';

//Drawer header
const TXT_8 = 'Administrador';

//Drawer body
const TXT_9 = 'Roles';
const TXT_10 = 'Asignación de roles';
const TXT_11 = 'Usuarios';

//Drawer footer
const TXT_12 = 'Salir';

//Home
const TXT_13 = 'Usuarios';
const TXT_14 = 'Nombre(s)';
const TXT_15 = 'Apellido(s)';
const TXT_16 = 'Correo';
const TXT_17 = 'No hay usuarios registrados';
const TXT_39 = 'Usuario eliminado';
const TXT_40 = 'Confirmación';
const TXT_41 = '¿Esta seguro(a) que desea eliminar el usuario con el identificador personId?';
const TXT_42 = 'No se pudo obtener los usuarios';

//RegisterPersonForm
const TXT_18 = 'Nombre(s)';
const TXT_19 = 'Apellido(s)';
const TXT_20 = 'Correo';
const TXT_21 = 'Nombre de usuario';
const TXT_22 = 'Contraseña';
const TXT_23 = 'Registrar';
const TXT_24 = 'Usuario registrado exitosamente';
const TXT_25 = 'Formulario de registro de usuario';
const TXT_26 = 'Registro de usuario';

//PersonViewer
const TXT_27 = 'Nombre(s)';
const TXT_28 = 'Apellido(s)';
const TXT_29 = 'Correo';
const TXT_30 = 'Nombre de usuario';
const TXT_31 = 'Detalle de usuario';

//UpdatePersonForm
const TXT_32 = 'Nombre(s)';
const TXT_33 = 'Apellido(s)';
const TXT_34 = 'Correo';
const TXT_35 = 'Nombre de usuario';
const TXT_36 = 'Editar';
const TXT_37 = 'Usuario editado exitosamente';
const TXT_38 = 'Edición de usuario';

//Roles
const TXT_43 = 'Roles';
const TXT_44 = 'Nombre';
const TXT_45 = 'Descripción';
const TXT_46 = 'No hay roles registrados';
const TXT_47 = 'Rol eliminado';
const TXT_48 = 'Confirmación';
const TXT_49 = '¿Esta seguro(a) que desea eliminar el rol con el identificador roleId?';
const TXT_50 = 'No se pudo obtener los roles';

//RegisterRoleForm
const TXT_51 = 'Nombre';
const TXT_52 = 'Descripción';
const TXT_53 = 'Registrar';
const TXT_54 = 'Rol registrado exitosamente';
const TXT_55 = 'Formulario de registro de rol';
const TXT_56 = 'Registro de rol';

//RoleViewer
const TXT_57 = 'Nombre';
const TXT_58 = 'Descripción';
const TXT_59 = 'Detalle de rol';

//UpdateRoleForm
const TXT_60 = 'Nombre';
const TXT_61 = 'Descripción';
const TXT_62 = 'Editar';
const TXT_63 = 'Rol editado exitosamente';
const TXT_64 = 'Edición de rol';

//RolesAllocation
const TXT_65 = 'Asignación de roles';

export {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
  TXT_14,
  TXT_15,
  TXT_16,
  TXT_17,
  TXT_18,
  TXT_19,
  TXT_20,
  TXT_21,
  TXT_22,
  TXT_23,
  TXT_24,
  TXT_25,
  TXT_26,
  TXT_27,
  TXT_28,
  TXT_29,
  TXT_30,
  TXT_31,
  TXT_32,
  TXT_33,
  TXT_34,
  TXT_35,
  TXT_36,
  TXT_37,
  TXT_38,
  TXT_39,
  TXT_40,
  TXT_41,
  TXT_42,
  TXT_43,
  TXT_44,
  TXT_45,
  TXT_46,
  TXT_47,
  TXT_48,
  TXT_49,
  TXT_50,
  TXT_51,
  TXT_52,
  TXT_53,
  TXT_54,
  TXT_55,
  TXT_56,
  TXT_57,
  TXT_58,
  TXT_59,
  TXT_60,
  TXT_61,
  TXT_62,
  TXT_63,
  TXT_64,
  TXT_65,
};
