/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
/****************
 * From project *
 ****************/
import { LOGIN } from 'config';

class PrivateRoute extends Component {

  constructor(props) {
    super(props);

    this._renderComponent = this.renderComponent.bind(this);
  }

  render() {

    return (
      <Route
        strict={this.props.strict}
        exact={this.props.exact}
        path={this.props.path}
        render={this._renderComponent}
      />
    );
  }

  renderComponent(props) {

    return (
      this.props.authenticated
      ?
        React.createElement(this.props.component, props)
      :
        <Redirect
          to={LOGIN}
          from={props.location.pathname}/>
    );
  }
}

PrivateRoute.propTypes = {
  strict: PropTypes.bool,
  exact: PropTypes.bool,
  path: PropTypes.string.isRequired,
  component: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired,
};

export default PrivateRoute;
