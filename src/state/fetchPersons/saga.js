/********************
 * From third party *
 ********************/
import { apply, put, takeLatest, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { FETCH_PERSONS_BY_PAGE_URI  } from '../../config';
import {
  FETCH_PERSONS,
  WILL_FETCH_PERSONS,
  FETCHING_PERSONS,
  DID_FETCH_PERSONS,
} from '../../common/actions';

function* fetchPersons({ payload }) {
  try {

    yield put(actionBuilder(WILL_FETCH_PERSONS));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.get, [FETCH_PERSONS_BY_PAGE_URI.replace('tk', session.id).replace('start', payload.start).replace('end', payload.end)]);
    yield put(actionBuilder(FETCHING_PERSONS, response, false));

  } catch (error) {
    yield put(actionBuilder(FETCHING_PERSONS, error, true));
  }

  yield put(actionBuilder(DID_FETCH_PERSONS));
}

function* saga() {
  yield takeLatest(FETCH_PERSONS, fetchPersons);
}

export default saga;
