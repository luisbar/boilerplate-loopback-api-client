/****************
 * From project *
 ****************/
import {
  FETCH_PERSONS,
  FETCHING_PERSONS,
  DID_FETCH_PERSONS,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  persons: [],
  statusOfFetchingPersons: {
    isFetchingPersons: false,
    wasAnError: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function fetchPersons(state = initialState, action) {

  switch (action.type) {

    case FETCH_PERSONS:
      return Object.assign({}, state, {
        statusOfFetchingPersons: {
          isFetchingPersons: true,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case FETCHING_PERSONS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfFetchingPersons: {
            isFetchingPersons: true,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

        return Object.assign({}, state, {
          persons: action.payload,
        });

    case DID_FETCH_PERSONS:
      return Object.assign({}, state, {
        persons: [],
        statusOfFetchingPersons: {
          isFetchingPersons: false,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
