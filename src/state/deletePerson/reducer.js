/****************
 * From project *
 ****************/
import {
  WILL_DELETE_PERSON,
  DELETING_PERSON,
  DID_DELETE_PERSON,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfDeletingPerson: {
    isDeletingPerson: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function deletePerson(state = initialState, action) {

  switch (action.type) {

    case WILL_DELETE_PERSON:
      return Object.assign({}, state, {
        statusOfDeletingPerson: {
          isDeletingPerson: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case DELETING_PERSON:
      if (action.error || !action.payload.count)
        return Object.assign({}, state, {
          statusOfDeletingPerson: {
            isDeletingPerson: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message || 'El usuario no existe',
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfDeletingPerson: {
            isDeletingPerson: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
            },
          },
        });

    case DID_DELETE_PERSON:
      return Object.assign({}, state, {
        statusOfDeletingPerson: {
          isDeletingPerson: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
