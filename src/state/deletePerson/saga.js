/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { DELETE_PERSON_URI  } from '../../config';
import {
  DELETE_PERSON,
  WILL_DELETE_PERSON,
  DELETING_PERSON,
  DID_DELETE_PERSON,
} from '../../common/actions';

function* deletePerson({ payload }) {
  try {

    yield put(actionBuilder(WILL_DELETE_PERSON));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.delete, [DELETE_PERSON_URI.replace('id', payload).replace('tk', session.id)]);
    yield put(actionBuilder(DELETING_PERSON, response, false));

  } catch (error) {
    yield put(actionBuilder(DELETING_PERSON, error, true));
  }

  yield put(actionBuilder(DID_DELETE_PERSON));
}

function* saga() {
  yield takeEvery(DELETE_PERSON, deletePerson);
}

export default saga;
