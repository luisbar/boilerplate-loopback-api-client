/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { FETCH_PERSON_URI  } from '../../config';
import {
  FETCH_PERSON,
  WILL_FETCH_PERSON,
  FETCHING_PERSON,
  DID_FETCH_PERSON,
} from '../../common/actions';

function* fetchPerson({ payload }) {
  try {

    yield put(actionBuilder(WILL_FETCH_PERSON));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.get, [FETCH_PERSON_URI.replace('id', payload).replace('tk', session.id)]);
    yield put(actionBuilder(FETCHING_PERSON, response, false));

  } catch (error) {
    yield put(actionBuilder(FETCHING_PERSON, error, true));
  }

  yield put(actionBuilder(DID_FETCH_PERSON));
}

function* saga() {
  yield takeEvery(FETCH_PERSON, fetchPerson);
}

export default saga;
