/****************
 * From project *
 ****************/
import {
  FETCH_PERSON,
  FETCHING_PERSON,
  DID_FETCH_PERSON,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  person: [],
  statusOfFetchingPerson: {
    isFetchingPerson: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function fetchPerson(state = initialState, action) {

  switch (action.type) {

    case FETCH_PERSON:
      return Object.assign({}, state, {
        statusOfFetchingPerson: {
          isFetchingPerson: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case FETCHING_PERSON:
      if (action.error)
        return Object.assign({}, state, {
          statusOfFetchingPerson: {
            isFetchingPerson: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

        return Object.assign({}, state, {
          person: action.payload,
          statusOfFetchingPerson: {
            isFetchingPerson: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
            },
          },
        });

    case DID_FETCH_PERSON:
      return Object.assign({}, state, {
        person: [],
        statusOfFetchingPerson: {
          isFetchingPerson: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
