/****************
 * From project *
 ****************/
import {
  WILL_SIGN_IN,
  SIGNIN_IN,
  DID_SIGN_IN,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfSigninIn: {
    isSigninIn: false,
    wasAnError: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function signIn(state = initialState, action) {

  switch (action.type) {

    case WILL_SIGN_IN:
      return Object.assign({}, state, {
        statusOfSigninIn: {
          isSigninIn: true,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case SIGNIN_IN:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSigninIn: {
            isSigninIn: true,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

      return state;

    case DID_SIGN_IN:
      return Object.assign({}, state, {
        statusOfSigninIn: {
          isSigninIn: false,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
