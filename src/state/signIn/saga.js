/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { SIGN_IN_URI  } from '../../config';
import {
  SIGN_IN,
  WILL_SIGN_IN,
  SIGNIN_IN,
  DID_SIGN_IN,
} from '../../common/actions';

function* signIn(action) {
  try {

    yield put(actionBuilder(WILL_SIGN_IN));
    const response = yield apply(requester, requester.post, [SIGN_IN_URI, action.payload]);
    yield call(sessionService.saveSession, response);
    yield put(actionBuilder(SIGNIN_IN, response, false));

  } catch (error) {
    yield put(actionBuilder(SIGNIN_IN, error, true));
  }

  yield put(actionBuilder(DID_SIGN_IN));
}

function* saga() {
  yield takeEvery(SIGN_IN, signIn);
}

export default saga;
