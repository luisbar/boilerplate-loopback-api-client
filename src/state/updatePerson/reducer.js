/****************
 * From project *
 ****************/
import {
  WILL_UPDATE_PERSON,
  UPDATING_PERSON,
  DID_UPDATE_PERSON,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfUpdatingPerson: {
    isUpdatingPerson: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export default function updatePerson(state = initialState, action) {

  switch (action.type) {

    case WILL_UPDATE_PERSON:
      return Object.assign({}, state, {
        statusOfUpdatingPerson: {
          isUpdatingPerson: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    case UPDATING_PERSON:
      if (action.error)
        return Object.assign({}, state, {
          statusOfUpdatingPerson: {
            isUpdatingPerson: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.details ? action.payload.details.messages : {},
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfUpdatingPerson: {
            isUpdatingPerson: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
              fieldErrorMessages: {},
            },
          },
        });

    case DID_UPDATE_PERSON:
      return Object.assign({}, state, {
        statusOfUpdatingPerson: {
          isUpdatingPerson: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    default:
      return state;
  }
};
