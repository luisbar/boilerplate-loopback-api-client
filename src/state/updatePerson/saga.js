/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { UPDATE_PERSON_URI  } from '../../config';
import {
  UPDATE_PERSON,
  WILL_UPDATE_PERSON,
  UPDATING_PERSON,
  DID_UPDATE_PERSON,
} from '../../common/actions';

function* updatePerson({ payload }) {
  try {

    yield put(actionBuilder(WILL_UPDATE_PERSON));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.patch, [UPDATE_PERSON_URI.replace('tk', session.id), payload]);
    yield put(actionBuilder(UPDATING_PERSON, response, false));

  } catch (error) {
    yield put(actionBuilder(UPDATING_PERSON, error, true));
  }

  yield put(actionBuilder(DID_UPDATE_PERSON));
}

function* saga() {
  yield takeEvery(UPDATE_PERSON, updatePerson);
}

export default saga;
