/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { SIGN_OUT_URI  } from '../../config';
import {
  SIGN_OUT,
  WILL_SIGN_OUT,
  SIGNIN_OUT,
  DID_SIGN_OUT,
} from '../../common/actions';

function* signOut() {
  try {

    yield put(actionBuilder(WILL_SIGN_OUT));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.post, [SIGN_OUT_URI.replace('tk', session.id)]);
    yield call(sessionService.deleteSession);
    yield put(actionBuilder(SIGNIN_OUT, response, false));

  } catch (error) {
    yield put(actionBuilder(SIGNIN_OUT, error, true));
  }

  yield put(actionBuilder(DID_SIGN_OUT));
}

function* saga() {
  yield takeEvery(SIGN_OUT, signOut);
}

export default saga;
