/****************
 * From project *
 ****************/
import {
  WILL_SIGN_OUT,
  SIGNIN_OUT,
  DID_SIGN_OUT,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfSigninOut: {
    isSigninOut: false,
    wasAnError: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function signOut(state = initialState, action) {

  switch (action.type) {

    case WILL_SIGN_OUT:
      return Object.assign({}, state, {
        statusOfSigninOut: {
          isSigninOut: true,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case SIGNIN_OUT:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSigninOut: {
            isSigninOut: true,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

      return state;

    case DID_SIGN_OUT:
      return Object.assign({}, state, {
        statusOfSigninOut: {
          isSigninOut: false,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
