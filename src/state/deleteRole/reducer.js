/****************
 * From project *
 ****************/
import {
  WILL_DELETE_ROLE,
  DELETING_ROLE,
  DID_DELETE_ROLE,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfDeletingRole: {
    isDeletingRole: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function deleteRole(state = initialState, action) {

  switch (action.type) {

    case WILL_DELETE_ROLE:
      return Object.assign({}, state, {
        statusOfDeletingRole: {
          isDeletingRole: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case DELETING_ROLE:
      if (action.error || !action.payload.count)
        return Object.assign({}, state, {
          statusOfDeletingRole: {
            isDeletingRole: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message || 'El rol no existe',
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfDeletingRole: {
            isDeletingRole: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
            },
          },
        });

    case DID_DELETE_ROLE:
      return Object.assign({}, state, {
        statusOfDeletingRole: {
          isDeletingRole: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
