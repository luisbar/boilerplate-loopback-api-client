/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { DELETE_ROLE_URI  } from '../../config';
import {
  DELETE_ROLE,
  WILL_DELETE_ROLE,
  DELETING_ROLE,
  DID_DELETE_ROLE,
} from '../../common/actions';

function* deleteRole({ payload }) {
  try {

    yield put(actionBuilder(WILL_DELETE_ROLE));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.delete, [DELETE_ROLE_URI.replace('id', payload).replace('tk', session.id)]);
    yield put(actionBuilder(DELETING_ROLE, response, false));

  } catch (error) {
    yield put(actionBuilder(DELETING_ROLE, error, true));
  }

  yield put(actionBuilder(DID_DELETE_ROLE));
}

function* saga() {
  yield takeEvery(DELETE_ROLE, deleteRole);
}

export default saga;
