/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { UPDATE_ROLE_URI  } from '../../config';
import {
  UPDATE_ROLE,
  WILL_UPDATE_ROLE,
  UPDATING_ROLE,
  DID_UPDATE_ROLE,
} from '../../common/actions';

function* updateRole({ payload }) {
  try {

    yield put(actionBuilder(WILL_UPDATE_ROLE));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.patch, [UPDATE_ROLE_URI.replace('tk', session.id), payload]);
    yield put(actionBuilder(UPDATING_ROLE, response, false));

  } catch (error) {
    yield put(actionBuilder(UPDATING_ROLE, error, true));
  }

  yield put(actionBuilder(DID_UPDATE_ROLE));
}

function* saga() {
  yield takeEvery(UPDATE_ROLE, updateRole);
}

export default saga;
