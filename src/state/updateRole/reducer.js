/****************
 * From project *
 ****************/
import {
  WILL_UPDATE_ROLE,
  UPDATING_ROLE,
  DID_UPDATE_ROLE,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfUpdatingRole: {
    isUpdatingRole: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export default function updateRole(state = initialState, action) {

  switch (action.type) {

    case WILL_UPDATE_ROLE:
      return Object.assign({}, state, {
        statusOfUpdatingRole: {
          isUpdatingRole: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    case UPDATING_ROLE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfUpdatingRole: {
            isUpdatingRole: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.details ? action.payload.details.messages : {},
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfUpdatingRole: {
            isUpdatingRole: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
              fieldErrorMessages: {},
            },
          },
        });

    case DID_UPDATE_ROLE:
      return Object.assign({}, state, {
        statusOfUpdatingRole: {
          isUpdatingRole: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    default:
      return state;
  }
};
