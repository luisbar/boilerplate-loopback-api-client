/********************
 * From third party *
 ********************/
import { combineReducers } from 'redux';
/****************
 * From project *
 ****************/
import signInReducer from './signIn/reducer';
import signOutReducer from './signOut/reducer';
import fetchPersonsReducer from './fetchPersons/reducer';
import savePersonReducer from './savePerson/reducer';
import fetchPersonReducer from './fetchPerson/reducer';
import updatePersonReducer from './updatePerson/reducer';
import deletePersonReducer from './deletePerson/reducer';
import fetchRolesReducer from './fetchRoles/reducer';
import deleteRoleReducer from './deleteRole/reducer';
import saveRoleReducer from './saveRole/reducer';
import fetchRoleReducer from './fetchRole/reducer';
import updateRoleReducer from './updateRole/reducer';

export default combineReducers({
  signInReducer,
  signOutReducer,
  fetchPersonsReducer,
  savePersonReducer,
  fetchPersonReducer,
  updatePersonReducer,
  deletePersonReducer,
  fetchRolesReducer,
  deleteRoleReducer,
  saveRoleReducer,
  fetchRoleReducer,
  updateRoleReducer,
});
