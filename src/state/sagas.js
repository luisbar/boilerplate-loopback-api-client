/********************
 * From third party *
 ********************/
import { all } from 'redux-saga/effects';
/****************
 * From project *
 ****************/
import signIn from './signIn/saga';
import signOut from './signOut/saga';
import fetchPersons from './fetchPersons/saga';
import savePerson from './savePerson/saga';
import fetchPerson from './fetchPerson/saga';
import updatePerson from './updatePerson/saga';
import deletePerson from './deletePerson/saga';
import fetchRoles from './fetchRoles/saga';
import deleteRole from './deleteRole/saga';
import saveRole from './saveRole/saga';
import fetchRole from './fetchRole/saga';
import updateRole from './updateRole/saga';

export default function* root() {
  yield all([
    signIn(),
    signOut(),
    fetchPersons(),
    savePerson(),
    fetchPerson(),
    updatePerson(),
    deletePerson(),
    fetchRoles(),
    deleteRole(),
    saveRole(),
    fetchRole(),
    updateRole(),
  ]);
};
