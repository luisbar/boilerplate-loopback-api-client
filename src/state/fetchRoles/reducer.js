/****************
 * From project *
 ****************/
import {
  FETCH_ROLES,
  FETCHING_ROLES,
  DID_FETCH_ROLES,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  roles: [],
  statusOfFetchingRoles: {
    isFetchingRoles: false,
    wasAnError: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function fetchRoles(state = initialState, action) {

  switch (action.type) {

    case FETCH_ROLES:
      return Object.assign({}, state, {
        statusOfFetchingRoles: {
          isFetchingRoles: true,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case FETCHING_ROLES:
      if (action.error)
        return Object.assign({}, state, {
          statusOfFetchingRoles: {
            isFetchingRoles: true,
            wasAnError: true,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

        return Object.assign({}, state, {
          roles: action.payload,
        });

    case DID_FETCH_ROLES:
      return Object.assign({}, state, {
        roles: [],
        statusOfFetchingRoles: {
          isFetchingRoles: false,
          wasAnError: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
