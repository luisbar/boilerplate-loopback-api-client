/********************
 * From third party *
 ********************/
import { apply, put, takeLatest, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { FETCH_ROLES_BY_PAGE_URI  } from '../../config';
import {
  FETCH_ROLES,
  WILL_FETCH_ROLES,
  FETCHING_ROLES,
  DID_FETCH_ROLES,
} from '../../common/actions';

function* fetchRoles({ payload }) {
  try {

    yield put(actionBuilder(WILL_FETCH_ROLES));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.get, [FETCH_ROLES_BY_PAGE_URI.replace('tk', session.id).replace('start', payload.start).replace('end', payload.end)]);
    yield put(actionBuilder(FETCHING_ROLES, response, false));

  } catch (error) {
    yield put(actionBuilder(FETCHING_ROLES, error, true));
  }

  yield put(actionBuilder(DID_FETCH_ROLES));
}

function* saga() {
  yield takeLatest(FETCH_ROLES, fetchRoles);
}

export default saga;
