/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { SAVE_PERSON_URI  } from '../../config';
import {
  SAVE_PERSON,
  WILL_SAVE_PERSON,
  SAVING_PERSON,
  DID_SAVE_PERSON,
} from '../../common/actions';

function* savePerson({ payload }) {
  try {

    yield put(actionBuilder(WILL_SAVE_PERSON));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.post, [SAVE_PERSON_URI.replace('tk', session.id), payload]);
    yield put(actionBuilder(SAVING_PERSON, response, false));

  } catch (error) {
    yield put(actionBuilder(SAVING_PERSON, error, true));
  }

  yield put(actionBuilder(DID_SAVE_PERSON));
}

function* saga() {
  yield takeEvery(SAVE_PERSON, savePerson);
}

export default saga;
