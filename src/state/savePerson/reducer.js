/****************
 * From project *
 ****************/
import {
  WILL_SAVE_PERSON,
  SAVING_PERSON,
  DID_SAVE_PERSON,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfSavingPerson: {
    isSavingPerson: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export default function savePerson(state = initialState, action) {

  switch (action.type) {

    case WILL_SAVE_PERSON:
      return Object.assign({}, state, {
        statusOfSavingPerson: {
          isSavingPerson: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    case SAVING_PERSON:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSavingPerson: {
            isSavingPerson: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.details ? action.payload.details.messages : {},
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfSavingPerson: {
            isSavingPerson: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
              fieldErrorMessages: {},
            },
          },
        });

    case DID_SAVE_PERSON:
      return Object.assign({}, state, {
        statusOfSavingPerson: {
          isSavingPerson: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    default:
      return state;
  }
};
