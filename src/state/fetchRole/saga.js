/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { FETCH_ROLE_URI  } from '../../config';
import {
  FETCH_ROLE,
  WILL_FETCH_ROLE,
  FETCHING_ROLE,
  DID_FETCH_ROLE,
} from '../../common/actions';

function* fetchRole({ payload }) {
  try {

    yield put(actionBuilder(WILL_FETCH_ROLE));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.get, [FETCH_ROLE_URI.replace('id', payload).replace('tk', session.id)]);
    yield put(actionBuilder(FETCHING_ROLE, response, false));

  } catch (error) {
    yield put(actionBuilder(FETCHING_ROLE, error, true));
  }

  yield put(actionBuilder(DID_FETCH_ROLE));
}

function* saga() {
  yield takeEvery(FETCH_ROLE, fetchRole);
}

export default saga;
