/****************
 * From project *
 ****************/
import {
  FETCH_ROLE,
  FETCHING_ROLE,
  DID_FETCH_ROLE,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  role: {},
  statusOfFetchingRole: {
    isFetchingRole: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
    },
  },
};

export default function fetchRole(state = initialState, action) {

  switch (action.type) {

    case FETCH_ROLE:
      return Object.assign({}, state, {
        statusOfFetchingRole: {
          isFetchingRole: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    case FETCHING_ROLE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfFetchingRole: {
            isFetchingRole: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
            },
          },
        });

        return Object.assign({}, state, {
          role: action.payload,
          statusOfFetchingRole: {
            isFetchingRole: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
            },
          },
        });

    case DID_FETCH_ROLE:
      return Object.assign({}, state, {
        role: {},
        statusOfFetchingRole: {
          isFetchingRole: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
          },
        },
      })

    default:
      return state;
  }
};
