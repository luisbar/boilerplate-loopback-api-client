/********************
 * From third party *
 ********************/
import { apply, put, takeEvery, call } from 'redux-saga/effects';
import { sessionService } from 'redux-react-session';
/****************
 * From project *
 ****************/
import actionBuilder from '../../common/actionBuilder';
import { requester } from '../../service/';
import { SAVE_ROLE_URI  } from '../../config';
import {
  SAVE_ROLE,
  WILL_SAVE_ROLE,
  SAVING_ROLE,
  DID_SAVE_ROLE,
} from '../../common/actions';

function* saveRole({ payload }) {
  try {

    yield put(actionBuilder(WILL_SAVE_ROLE));
    const session = yield call(sessionService.loadSession);
    const response = yield apply(requester, requester.post, [SAVE_ROLE_URI.replace('tk', session.id), payload]);
    yield put(actionBuilder(SAVING_ROLE, response, false));

  } catch (error) {
    yield put(actionBuilder(SAVING_ROLE, error, true));
  }

  yield put(actionBuilder(DID_SAVE_ROLE));
}

function* saga() {
  yield takeEvery(SAVE_ROLE, saveRole);
}

export default saga;
