/****************
 * From project *
 ****************/
import {
  WILL_SAVE_ROLE,
  SAVING_ROLE,
  DID_SAVE_ROLE,
} from '../../common/actions';
/*************
 * Constants *
 *************/
const initialState = {
  statusOfSavingRole: {
    isSavingRole: false,
    wasAnError: false,
    wasASuccess: false,
    error: {
      errorMessage: '',
      fieldErrorMessages: {},
    },
  },
};

export default function saveRole(state = initialState, action) {

  switch (action.type) {

    case WILL_SAVE_ROLE:
      return Object.assign({}, state, {
        statusOfSavingRole: {
          isSavingRole: true,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    case SAVING_ROLE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSavingRole: {
            isSavingRole: true,
            wasAnError: true,
            wasASuccess: false,
            error: {
              errorMessage: action.payload.message,
              fieldErrorMessages: action.payload.details ? action.payload.details.messages : {},
            },
          },
        });
      else
        return Object.assign({}, state, {
          statusOfSavingRole: {
            isSavingRole: true,
            wasAnError: false,
            wasASuccess: true,
            error: {
              errorMessage: '',
              fieldErrorMessages: {},
            },
          },
        });

    case DID_SAVE_ROLE:
      return Object.assign({}, state, {
        statusOfSavingRole: {
          isSavingRole: false,
          wasAnError: false,
          wasASuccess: false,
          error: {
            errorMessage: '',
            fieldErrorMessages: {},
          },
        },
      })

    default:
      return state;
  }
};
