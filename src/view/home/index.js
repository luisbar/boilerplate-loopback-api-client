/********************
 * From third party *
 ********************/
import React from 'react';
import Box from 'grommet/components/Box';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import BasisComponent from 'basisComponent';
import { CrudTable, ConfirmationModal, Alert } from 'component/';
import actionBuilder from 'common/actionBuilder';
import { FETCH_PERSONS, DELETE_PERSON } from 'common/actions';
import { TXT_13, TXT_14, TXT_15, TXT_16, TXT_17, TXT_39, TXT_40, TXT_41, TXT_42 } from 'string';
import { REGISTER_PERSON_FORM, PERSON_VIEWER, UPDATE_PERSON_FORM } from 'config';
/*************
 * Constants *
 *************/
const rowsByPage = 20;

class Home extends BasisComponent {

  constructor(props) {
    super(props);

    this.personId = null;

    this._onFetchPersons = this.onFetchPersons.bind(this);
    this._onView = this.onView.bind(this);
    this._onEdit = this.onEdit.bind(this);
    this._onDelete = this.onDelete.bind(this);
    this._onAdd = this.onAdd.bind(this);
    this._onCancel = this.onCancel.bind(this);
    this._onAccept = this.onAccept.bind(this);

    this.state = {
      start: 0,
      end: rowsByPage,
      confirmationModalIsHidden: true,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.persons && !!nextProps.persons.length)
      return ({
        start: nextProps.persons.length === rowsByPage ? prevState.end : prevState.start + nextProps.persons.length,
        end: nextProps.persons.length === rowsByPage ? prevState.end + rowsByPage : prevState.end + nextProps.persons.length
      });

    return null;
  }

  componentDidMount() {
    super.componentDidMount();
    this.onFetchPersons();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.wasASuccess)
      this.refs.crudTable.deletePerson(this.personId);
  }

  render() {

    return (
      <Box
        id={'home'}
        flex={true}
        justify={'center'}
        align={'center'}>

        <Alert
          status={this.props.wasAnError || this.props.isFetchingPersonsWasAnError ? 'critical' : 'ok'}
          visibility={this.props.wasAnError || this.props.wasASuccess || this.props.isFetchingPersonsWasAnError}
          message={this.props.wasAnError ? this.props.errorMessage : this.props.wasASuccess ? TXT_39 : TXT_42}/>

        <CrudTable
          ref={'crudTable'}
          title={TXT_13}
          header={[TXT_14, TXT_15, TXT_16]}
          data={this.props.persons}
          emptyText={TXT_17}
          fieldsToShow={['firstName', 'lastName', 'email']}
          onFetchMoreData={this._onFetchPersons}
          onView={this._onView}
          onEdit={this._onEdit}
          onDelete={this._onDelete}
          fetchingData={this.props.isFetchingPersons}
          onAdd={this._onAdd}/>

        <ConfirmationModal
          align={'top'}
          hidden={this.state.confirmationModalIsHidden}
          title={TXT_40}
          description={TXT_41.replace('personId', this.personId)}
          onCancelButtonClicked={this._onCancel}
          onAcceptButtonClicked={this._onAccept}/>
      </Box>
    );
  }

  onFetchPersons() {
    this.props.fetchPersons(this.state.start, this.state.end);
  }

  onView(personId) {
    this.props.goToPersonViewer(personId);
  }

  onEdit(personId) {
    this.props.goToUpdatePersonForm(personId);
  }

  onDelete(personId) {
    this.personId = personId;
    this.setState({ confirmationModalIsHidden: false });
  }

  onAdd() {
    this.props.goToRegisterPersonForm();
  }

  onCancel() {
    this.setState({ confirmationModalIsHidden: true });
  }

  onAccept() {
    this.setState({ confirmationModalIsHidden: true });
    this.props.deleteItem(this.personId);
  }
}

const mapStateToProps = state => ({
  persons: state
    .rootReducer
    .stateReducer
    .fetchPersonsReducer
    .persons,
  isFetchingPersons: state
    .rootReducer
    .stateReducer
    .fetchPersonsReducer
    .statusOfFetchingPersons
    .isFetchingPersons,
  isFetchingPersonsWasAnError: state
    .rootReducer
    .stateReducer
    .fetchPersonsReducer
    .statusOfFetchingPersons
    .wasAnError,
  wasAnError: state
    .rootReducer
    .stateReducer
    .deletePersonReducer
    .statusOfDeletingPerson
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .deletePersonReducer
    .statusOfDeletingPerson
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .deletePersonReducer
    .statusOfDeletingPerson
    .error
    .errorMessage,
});

const mapDispatchToProps = dispatch => ({
  fetchPersons: (start, end) => dispatch(actionBuilder(FETCH_PERSONS, { start, end })),
  deletePerson: (personId) => dispatch(actionBuilder(DELETE_PERSON, personId)),
  goToRegisterPersonForm: () => dispatch(replace(REGISTER_PERSON_FORM)),
  goToPersonViewer: (id) => dispatch(replace(PERSON_VIEWER.replace(':id', id))),
  goToUpdatePersonForm: (id) => dispatch(replace(UPDATE_PERSON_FORM.replace(':id', id))),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Home));
