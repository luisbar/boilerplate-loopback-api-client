/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { FETCH_ROLE } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { ROLES } from 'config';
import { TXT_57, TXT_58, TXT_59 } from 'string';

class RoleViewer extends Component {

  constructor(props) {
    super(props);

    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: this.getBody(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.wasASuccess && nextProps.isFetchingRole)
      prevState.body.inputs.map((input, index) =>
        input.field.value = nextProps.role[input.id]
      );

    return null;
  }

  componentDidMount() {
    this.props.fetchRole(this.props.match.params.id);
  }

  render() {

    return (
      <Box
        flex
        id={'roleViewer'}>

        <ToolBar
          title={TXT_59}
          onBackButtonClicked={this._onBackButtonClicked}/>

        <Alert
          status={'critical'}
          visibility={this.props.wasAnError}
          message={this.props.errorMessage}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          <JsonForm
            ref={'form'}
            container={this.getContainer()}
            header={this.getHeader()}
            body={this.state.body}
            footer={this.getFooter()}/>
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
    };
  }

  getBody() {

    return {
      inputs: [
        {
          id: 'name',
          type: 'textField',
          fieldContainer: {
            label: TXT_57,
          },
          field: {
            disabled: true,
          }
        },
        {
          id: 'description',
          type: 'textAreaField',
          fieldContainer: {
            label: TXT_58,
          },
          field: {
            rows: 5,
            disabled: true,
          }
        },
      ]
    };
  }

  getHeader() {

    return {
      title: this.props.match.params.id,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
    };
  }

  onBackButtonClicked() {
    this.props.goToRoles();
  }
}

const mapStateToProps = state => ({
  isFetchingRole: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .isFetchingRole,
  wasAnError: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .error
    .errorMessage,
  role: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .role,
});

const mapDispatchToProps = dispatch => ({
  fetchRole: (roleId) => dispatch(actionBuilder(FETCH_ROLE, roleId)),
  goToRoles: () => dispatch(replace(ROLES)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(RoleViewer));
