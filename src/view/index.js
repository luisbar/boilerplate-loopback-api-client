import UpdateRoleForm from 'view/updateRoleForm';
import RoleViewer from 'view/roleViewer';
import RegisterRoleForm from 'view/registerRoleForm';
import UpdatePersonForm from 'view/updatePersonForm';
import RolesAllocation from 'view/rolesAllocation';
import Roles from 'view/roles';
import PersonViewer from 'view/personViewer';
import RegisterPersonForm from 'view/registerPersonForm';
import Home from 'view/home';
import Login from 'view/login';
import NotFound from 'view/notFound';

export {
  UpdateRoleForm,
  RoleViewer,
  RegisterRoleForm,
  UpdatePersonForm,
  RolesAllocation,
  Roles,
  PersonViewer,
  RegisterPersonForm,
  Home,
  Login,
  NotFound,
};
