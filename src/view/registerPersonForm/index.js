/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Spinning from 'grommet/components/icons/Spinning';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { SAVE_PERSON } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { HOME } from 'config';
import { TXT_18, TXT_19, TXT_20, TXT_21, TXT_22, TXT_23, TXT_24, TXT_25, TXT_26 } from 'string';

class RegisterPersonForm extends Component {

  constructor(props) {
    super(props);

    this._onAddUser = this.onAddUser.bind(this);
    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: this.getBody(),
      footer: this.getFooter(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //To show a spinning
    if (nextProps.isSavingPerson)
      prevState.footer.button.icon = <Spinning size={'xsmall'}/>;
    else
      delete prevState.footer.button.icon;
    //To show fieldErrorMessages
    if (nextProps.wasAnError && nextProps.isSavingPerson)
      prevState.body.inputs.map((input, index) =>
        input.errors = nextProps.fieldErrorMessages[input.id]
      );
    //To delete fieldErrorMessages
    if (nextProps.wasASuccess && nextProps.isSavingPerson)
      prevState.body.inputs.map((input, index) =>
        input.errors = []
      );

    return null;
  }

  render() {

    return (
      <Box
        flex
        id={'registerPersonForm'}>

        <ToolBar
          title={TXT_26}
          onBackButtonClicked={this._onBackButtonClicked}/>

        <Alert
          status={this.props.wasAnError ? 'critical' : 'ok'}
          visibility={this.props.wasAnError || this.props.wasASuccess}
          message={this.props.errorMessage || TXT_24}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          <JsonForm
            ref={'form'}
            container={this.getContainer()}
            header={this.getHeader()}
            body={this.state.body}
            footer={this.state.footer}/>
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
      onSubmit: this._onAddUser
    };
  }

  getBody() {

    return {
      inputs: [
        {
          id: 'firstName',
          type: 'textField',
          fieldContainer: {
            label: TXT_18,
          },
          field: {
          }
        },
        {
          id: 'lastName',
          type: 'textField',
          fieldContainer: {
            label: TXT_19,
          },
          field: {
          }
        },
        {
          id: 'email',
          type: 'textField',
          fieldContainer: {
            label: TXT_20,
          },
          field: {
          }
        },
        {
          id: 'username',
          type: 'textField',
          fieldContainer: {
            label: TXT_21,
          },
          field: {
          }
        },
        {
          id: 'password',
          type: 'passwordField',
          fieldContainer: {
            label: TXT_22,
          },
          field: {
          }
        },
      ]
    };
  }

  getHeader() {

    return {
      title: TXT_25,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
      button: {
        label: TXT_23,
        fill: true,
        type: 'submit',
        accent: true,
      },
    };
  }

  onAddUser(evt) {
    evt.preventDefault();
    this.props.savePerson(this.refs.form.getInputValues());
  }

  onBackButtonClicked() {
    this.props.goToHome();
  }
}

const mapStateToProps = state => ({
  isSavingPerson: state
    .rootReducer
    .stateReducer
    .savePersonReducer
    .statusOfSavingPerson
    .isSavingPerson,
  wasAnError: state
    .rootReducer
    .stateReducer
    .savePersonReducer
    .statusOfSavingPerson
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .savePersonReducer
    .statusOfSavingPerson
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .savePersonReducer
    .statusOfSavingPerson
    .error
    .errorMessage,
  fieldErrorMessages: state
    .rootReducer
    .stateReducer
    .savePersonReducer
    .statusOfSavingPerson
    .error
    .fieldErrorMessages,
});

const mapDispatchToProps = dispatch => ({
  savePerson: (data) => dispatch(actionBuilder(SAVE_PERSON, data)),
  goToHome: () => dispatch(replace(HOME)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPersonForm));
