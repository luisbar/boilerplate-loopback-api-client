/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
/****************
 * From project *
 ****************/
import { ToolBar } from 'component/';
import { TXT_65 } from 'string';

class RolesAllocation extends Component {

  render() {

    return (
      <Box>
        <ToolBar
          title={TXT_65}/>
      </Box>
    );
  }
}


export default RolesAllocation;
