/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { FETCH_PERSON } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { HOME } from 'config';
import { TXT_27, TXT_28, TXT_29, TXT_30, TXT_31 } from 'string';

class PersonViewer extends Component {

  constructor(props) {
    super(props);

    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: this.getBody(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.wasASuccess && nextProps.isFetchingPerson)
      prevState.body.inputs.map((input, index) =>
        input.field.value = nextProps.person[input.id]
      );

    return null;
  }

  componentDidMount() {
    this.props.fetchPerson(this.props.match.params.id);
  }

  render() {

    return (
      <Box
        flex
        id={'personViewer'}>

        <ToolBar
          title={TXT_31}
          onBackButtonClicked={this._onBackButtonClicked}/>

        <Alert
          status={'critical'}
          visibility={this.props.wasAnError}
          message={this.props.errorMessage}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          <JsonForm
            ref={'form'}
            container={this.getContainer()}
            header={this.getHeader()}
            body={this.state.body}
            footer={this.getFooter()}/>
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
    };
  }

  getBody() {

    return {
      inputs: [
        {
          id: 'firstName',
          type: 'textField',
          fieldContainer: {
            label: TXT_27,
          },
          field: {
            disabled: true,
          }
        },
        {
          id: 'lastName',
          type: 'textField',
          fieldContainer: {
            label: TXT_28,
          },
          field: {
            disabled: true,
          }
        },
        {
          id: 'email',
          type: 'textField',
          fieldContainer: {
            label: TXT_29,
          },
          field: {
            disabled: true,
          }
        },
        {
          id: 'username',
          type: 'textField',
          fieldContainer: {
            label: TXT_30,
          },
          field: {
            disabled: true,
          }
        },
      ]
    };
  }

  getHeader() {

    return {
      title: this.props.match.params.id,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
    };
  }

  onBackButtonClicked() {
    this.props.goToHome();
  }
}

const mapStateToProps = state => ({
  isFetchingPerson: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .isFetchingPerson,
  wasAnError: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .error
    .errorMessage,
  person: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .person,
});

const mapDispatchToProps = dispatch => ({
  fetchPerson: (personId) => dispatch(actionBuilder(FETCH_PERSON, personId)),
  goToHome: () => dispatch(replace(HOME)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonViewer));
