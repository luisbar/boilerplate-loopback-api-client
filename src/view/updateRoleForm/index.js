/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Spinning from 'grommet/components/icons/Spinning';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { FETCH_ROLE, UPDATE_ROLE } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { ROLES } from 'config';
import { TXT_60, TXT_61, TXT_62, TXT_63, TXT_64 } from 'string';
/*************
 * Constants *
 *************/
let body = {
  inputs: [
    {
      id: 'name',
      type: 'textField',
      fieldContainer: {
        label: TXT_60,
      },
      field: {
      }
    },
    {
      id: 'description',
      type: 'textAreaField',
      fieldContainer: {
        label: TXT_61,
      },
      field: {
        rows: 5,
      }
    },
  ]
};

class UpdateRoleForm extends Component {

  constructor(props) {
    super(props);

    this._onEditRole = this.onEditRole.bind(this);
    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: null,
      footer: this.getFooter(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //To show a spinning
    if (nextProps.isUpdatingRole)
      prevState.footer.button.icon = <Spinning size={'xsmall'}/>;
    else
      delete prevState.footer.button.icon;
    //To show fieldErrorMessages
    if (nextProps.wasAnError && nextProps.isUpdatingRole)
      prevState.body.inputs.map((input, index) =>
        input.errors = nextProps.fieldErrorMessages[input.id]
      );
    //To delete fieldErrorMessages
    if (nextProps.wasASuccess && nextProps.isUpdatingRole)
      prevState.body.inputs.map((input, index) =>
        input.errors = []
      );
    //To set fetched data
    if (nextProps.fetchingRoleWasASuccess && nextProps.isFetchingRole) {
      body.inputs.map((input, index) =>
        input.field.defaultValue = nextProps.role[input.id]
      );

      return {
        body: body,
      };
    }

    return null
  }

  componentDidMount() {
    this.props.fetchRole(this.props.match.params.id);
  }

  render() {

    return (
      <Box
        flex
        id={'roleViewer'}>

        <ToolBar
          title={TXT_64}
          onBackButtonClicked={this._onBackButtonClicked}/>

          <Alert
            status={this.props.wasAnError ? 'critical' : 'ok'}
            visibility={this.props.fetchingRoleWasAnError || this.props.wasAnError || this.props.wasASuccess}
            message={this.props.errorMessage || TXT_63}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          {
            this.state.body
            ? <JsonForm
              ref={'form'}
              container={this.getContainer()}
              header={this.getHeader()}
              body={this.state.body}
              footer={this.getFooter()}/>
            : null
          }
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
      onSubmit: this._onEditRole
    };
  }

  getHeader() {

    return {
      title: this.props.match.params.id,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
      button: {
        label: TXT_62,
        fill: true,
        type: 'submit',
        accent: true,
      },
    };
  }

  onEditRole(evt) {
    evt.preventDefault();
    this.props.updateRole({...this.refs.form.getInputValues(), id: this.props.match.params.id});
  }

  onBackButtonClicked() {
    this.props.goToRoles();
  }
}

const mapStateToProps = state => ({
  isFetchingRole: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .isFetchingRole,
  fetchingRoleWasAnError: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .wasAnError,
  fetchingRoleWasASuccess: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .wasASuccess,
  fetchingRoleErrorMessage: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .statusOfFetchingRole
    .error
    .errorMessage,
  role: state
    .rootReducer
    .stateReducer
    .fetchRoleReducer
    .role,
  isUpdatingRole: state
    .rootReducer
    .stateReducer
    .updateRoleReducer
    .statusOfUpdatingRole
    .isUpdatingRole,
  wasAnError: state
    .rootReducer
    .stateReducer
    .updateRoleReducer
    .statusOfUpdatingRole
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .updateRoleReducer
    .statusOfUpdatingRole
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .updateRoleReducer
    .statusOfUpdatingRole
    .error
    .errorMessage,
  fieldErrorMessages: state
    .rootReducer
    .stateReducer
    .updateRoleReducer
    .statusOfUpdatingRole
    .error
    .fieldErrorMessages,
});

const mapDispatchToProps = dispatch => ({
  fetchRole: (roleId) => dispatch(actionBuilder(FETCH_ROLE, roleId)),
  updateRole: (data) => dispatch(actionBuilder(UPDATE_ROLE, data)),
  goToRoles: () => dispatch(replace(ROLES)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateRoleForm));
