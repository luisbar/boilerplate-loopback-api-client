/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Spinning from 'grommet/components/icons/Spinning';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { FETCH_PERSON, UPDATE_PERSON } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { HOME } from 'config';
import { TXT_32, TXT_33, TXT_34, TXT_35, TXT_36, TXT_37, TXT_38 } from 'string';
/*************
 * Constants *
 *************/
let body = {
  inputs: [
    {
      id: 'firstName',
      type: 'textField',
      fieldContainer: {
        label: TXT_32,
      },
      field: {
      }
    },
    {
      id: 'lastName',
      type: 'textField',
      fieldContainer: {
        label: TXT_33,
      },
      field: {
      }
    },
    {
      id: 'email',
      type: 'textField',
      fieldContainer: {
        label: TXT_34,
      },
      field: {
      }
    },
    {
      id: 'username',
      type: 'textField',
      fieldContainer: {
        label: TXT_35,
      },
      field: {
      }
    },
  ]
};

class UpdatePersonForm extends Component {

  constructor(props) {
    super(props);

    this._onEditUser = this.onEditUser.bind(this);
    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: null,
      footer: this.getFooter(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //To show a spinning
    if (nextProps.isUpdatingPerson)
      prevState.footer.button.icon = <Spinning size={'xsmall'}/>;
    else
      delete prevState.footer.button.icon;
    //To show fieldErrorMessages
    if (nextProps.wasAnError && nextProps.isUpdatingPerson)
      prevState.body.inputs.map((input, index) =>
        input.errors = nextProps.fieldErrorMessages[input.id]
      );
    //To delete fieldErrorMessages
    if (nextProps.wasASuccess && nextProps.isUpdatingPerson)
      prevState.body.inputs.map((input, index) =>
        input.errors = []
      );
    //To set fetched data
    if (nextProps.fetchingPersonWasASuccess && nextProps.isFetchingPerson) {
      body.inputs.map((input, index) =>
        input.field.defaultValue = nextProps.person[input.id]
      );

      return {
        body: body,
      };
    }

    return null
  }

  componentDidMount() {
    this.props.fetchPerson(this.props.match.params.id);
  }

  render() {

    return (
      <Box
        flex
        id={'personViewer'}>

        <ToolBar
          title={TXT_38}
          onBackButtonClicked={this._onBackButtonClicked}/>

          <Alert
            status={this.props.wasAnError ? 'critical' : 'ok'}
            visibility={this.props.fetchingPersonWasAnError || this.props.wasAnError || this.props.wasASuccess}
            message={this.props.errorMessage || TXT_37}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          {
            this.state.body
            ? <JsonForm
              ref={'form'}
              container={this.getContainer()}
              header={this.getHeader()}
              body={this.state.body}
              footer={this.getFooter()}/>
            : null
          }
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
      onSubmit: this._onEditUser
    };
  }

  getHeader() {

    return {
      title: this.props.match.params.id,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
      button: {
        label: TXT_36,
        fill: true,
        type: 'submit',
        accent: true,
      },
    };
  }

  onEditUser(evt) {
    evt.preventDefault();
    this.props.updatePerson({...this.refs.form.getInputValues(), id: this.props.match.params.id});
  }

  onBackButtonClicked() {
    this.props.goToHome();
  }
}

const mapStateToProps = state => ({
  isFetchingPerson: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .isFetchingPerson,
  fetchingPersonWasAnError: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .wasAnError,
  fetchingPersonWasASuccess: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .wasASuccess,
  fetchingPersonErrorMessage: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .statusOfFetchingPerson
    .error
    .errorMessage,
  person: state
    .rootReducer
    .stateReducer
    .fetchPersonReducer
    .person,
  isUpdatingPerson: state
    .rootReducer
    .stateReducer
    .updatePersonReducer
    .statusOfUpdatingPerson
    .isUpdatingPerson,
  wasAnError: state
    .rootReducer
    .stateReducer
    .updatePersonReducer
    .statusOfUpdatingPerson
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .updatePersonReducer
    .statusOfUpdatingPerson
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .updatePersonReducer
    .statusOfUpdatingPerson
    .error
    .errorMessage,
  fieldErrorMessages: state
    .rootReducer
    .stateReducer
    .updatePersonReducer
    .statusOfUpdatingPerson
    .error
    .fieldErrorMessages,
});

const mapDispatchToProps = dispatch => ({
  fetchPerson: (personId) => dispatch(actionBuilder(FETCH_PERSON, personId)),
  updatePerson: (data) => dispatch(actionBuilder(UPDATE_PERSON, data)),
  goToHome: () => dispatch(replace(HOME)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePersonForm));
