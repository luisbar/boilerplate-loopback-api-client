/********************
 * From third party *
 ********************/
import React from 'react';
import Box from 'grommet/components/Box';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import BasisComponent from 'basisComponent';
import { CrudTable, ConfirmationModal, Alert } from 'component/';
import actionBuilder from 'common/actionBuilder';
import { FETCH_ROLES, DELETE_ROLE } from 'common/actions';
import { TXT_43, TXT_44, TXT_45, TXT_46, TXT_47, TXT_48, TXT_49, TXT_50 } from 'string';
import { REGISTER_ROLE_FORM, ROLE_VIEWER, UPDATE_ROLE_FORM } from 'config';
/*************
 * Constants *
 *************/
const rowsByPage = 20;

class Roles extends BasisComponent {

  constructor(props) {
    super(props);

    this.roleId = null;

    this._onFetchRoles = this.onFetchRoles.bind(this);
    this._onView = this.onView.bind(this);
    this._onEdit = this.onEdit.bind(this);
    this._onDelete = this.onDelete.bind(this);
    this._onAdd = this.onAdd.bind(this);
    this._onCancel = this.onCancel.bind(this);
    this._onAccept = this.onAccept.bind(this);

    this.state = {
      start: 0,
      end: rowsByPage,
      confirmationModalIsHidden: true,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.roles && !!nextProps.roles.length)
      return ({
        start: nextProps.roles.length === rowsByPage ? prevState.end : prevState.start + nextProps.roles.length,
        end: nextProps.roles.length === rowsByPage ? prevState.end + rowsByPage : prevState.end + nextProps.roles.length
      });

    return null;
  }

  componentDidMount() {
    super.componentDidMount();
    this.onFetchRoles();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.wasASuccess)
      this.refs.crudTable.deleteItem(this.roleId);
  }

  render() {

    return (
      <Box
        id={'home'}
        flex={true}
        justify={'center'}
        align={'center'}>

        <Alert
          status={this.props.wasAnError || this.props.isFetchingRolesWasAnError ? 'critical' : 'ok'}
          visibility={this.props.wasAnError || this.props.wasASuccess || this.props.isFetchingRolesWasAnError}
          message={this.props.wasAnError ? this.props.errorMessage : this.props.wasASuccess ? TXT_47 : TXT_50}/>

        <CrudTable
          ref={'crudTable'}
          title={TXT_43}
          header={[TXT_44, TXT_45]}
          data={this.props.roles}
          emptyText={TXT_46}
          fieldsToShow={['name', 'description']}
          onFetchMoreData={this._onFetchRoles}
          onView={this._onView}
          onEdit={this._onEdit}
          onDelete={this._onDelete}
          fetchingData={this.props.isFetchingRoles}
          onAdd={this._onAdd}/>

        <ConfirmationModal
          align={'top'}
          hidden={this.state.confirmationModalIsHidden}
          title={TXT_48}
          description={TXT_49.replace('roleId', this.roleId)}
          onCancelButtonClicked={this._onCancel}
          onAcceptButtonClicked={this._onAccept}/>
      </Box>
    );
  }

  onFetchRoles() {
    this.props.fetchRoles(this.state.start, this.state.end);
  }

  onView(roleId) {
    this.props.goToRoleViewer(roleId);
  }

  onEdit(roleId) {
    this.props.goToUpdateRoleForm(roleId);
  }

  onDelete(roleId) {
    this.roleId = roleId;
    this.setState({ confirmationModalIsHidden: false });
  }

  onAdd() {
    this.props.goToRegisterRoleForm();
  }

  onCancel() {
    this.setState({ confirmationModalIsHidden: true });
  }

  onAccept() {
    this.setState({ confirmationModalIsHidden: true });
    this.props.deleteRole(this.roleId);
  }
}

const mapStateToProps = state => ({
  roles: state
    .rootReducer
    .stateReducer
    .fetchRolesReducer
    .roles,
  isFetchingRoles: state
    .rootReducer
    .stateReducer
    .fetchRolesReducer
    .statusOfFetchingRoles
    .isFetchingRoles,
  isFetchingRolesWasAnError: state
    .rootReducer
    .stateReducer
    .fetchRolesReducer
    .statusOfFetchingRoles
    .wasAnError,
  wasAnError: state
    .rootReducer
    .stateReducer
    .deleteRoleReducer
    .statusOfDeletingRole
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .deleteRoleReducer
    .statusOfDeletingRole
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .deleteRoleReducer
    .statusOfDeletingRole
    .error
    .errorMessage,
});

const mapDispatchToProps = dispatch => ({
  fetchRoles: (start, end) => dispatch(actionBuilder(FETCH_ROLES, { start, end })),
  deleteRole: (roleId) => dispatch(actionBuilder(DELETE_ROLE, roleId)),
  goToRegisterRoleForm: () => dispatch(replace(REGISTER_ROLE_FORM)),
  goToRoleViewer: (id) => dispatch(replace(ROLE_VIEWER.replace(':id', id))),
  goToUpdateRoleForm: (id) => dispatch(replace(UPDATE_ROLE_FORM.replace(':id', id))),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Roles));
