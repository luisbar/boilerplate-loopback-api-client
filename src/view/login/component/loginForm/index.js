/********************
 * From third party *
 ********************/
import React from 'react';
import Box from 'grommet/components/Box';
import Form from 'grommet/components/Form';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
/****************
 * From project *
 ****************/
import BasisComponent from 'basisComponent';
import { FormTitle, FormButton } from './component/';
import { TXT_5, TXT_6 } from 'string';

class LoginForm extends BasisComponent {

  constructor(props) {
    super(props);

    this.state = {
      username: null,
      password: null,
    };

    this._onTextChanged = (key) => this.onTextChanged.bind(this, key);
  }

  render() {

    return (
      <Box
        colorIndex={'accent-2'}>

        <FormTitle/>

        {this.renderForm()}

        <FormButton
          username={this.state.username}
          password={this.state.password}/>
      </Box>
    );
  }

  renderForm() {

    return (
      <Form
        plain={this.state.isSmartphone}
        pad={'medium'}>

        <FormField
          label={TXT_5}>
          <TextInput
            onDOMChange={this._onTextChanged('username')}/>
        </FormField>

        <FormField
          label={TXT_6}>
          <TextInput
            type={'password'}
            onDOMChange={this._onTextChanged('password')}/>
        </FormField>
      </Form>
    );
  }

  onTextChanged(key, evt) {
    this.setState({ [key]:  evt.target.value });
  }
}

export default LoginForm;
