/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Spinning from 'grommet/components/icons/Spinning';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
/****************
 * From project *
 ****************/
import { TXT_7 } from 'string';
import { SIGN_IN } from 'common/actions';
import actionBuilder from 'common/actionBuilder';

class FormButton extends Component {

  constructor(props) {
    super(props);

    this._onButtonClicked = this.onButtonClicked.bind(this);
  }

  render() {

    return (
      <Box
        align={'center'}
        pad={'medium'}>
        <Button
          label={TXT_7}
          accent={true}
          type={'button'}
          onClick={this._onButtonClicked}
          icon={this.renderIcon()}/>
      </Box>
    );
  }

  renderIcon() {

    return (
      this.props.signInIsRunning
      ? <Spinning/>
      : null
    );
  }

  onButtonClicked() {
    this.props.signIn(this.props.username, this.props.password);
  }
}

FormButton.propTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
};

const mapStateToProps = state => ({
  signInIsRunning: state
    .rootReducer
    .stateReducer
    .signInReducer
    .statusOfSigninIn
    .isSigninIn,
});

const mapDispatchToProps = dispatch => ({
  signIn: (username, password) => dispatch(actionBuilder(SIGN_IN, { username: username, password: password })),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(FormButton));
