import FormTitle from './formTitle';
import FormButton from './formButton';

export {
  FormTitle,
  FormButton,
};
