/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Heading from 'grommet/components/Heading';
/****************
 * From project *
 ****************/
import { TXT_4 } from 'string';

class FormTitle extends Component {

  render() {

    return (
      <Heading
        align={'center'}>
        {TXT_4}
      </Heading>
    );
  }
}

export default FormTitle;
