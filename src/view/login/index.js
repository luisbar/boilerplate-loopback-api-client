/********************
 * From third party *
 ********************/
import React from 'react';
import Box from 'grommet/components/Box';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux'
/****************
 * From project *
 ****************/
import BasisComponent from 'basisComponent';
import { LoginForm } from './component/';
import { Alert } from 'component/';
import { HOME } from 'config';

class Login extends BasisComponent {

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.authenticated)
      nextProps.goHome();

    return null;
  }

  render() {

    return (
      <Box
        id={'login'}
        flex={true}
        justify={'center'}
        align={'center'}>

        <Alert
          status={'critical'}
          visibility={this.props.wasAnError}
          message={this.props.errorMessage}/>

        <LoginForm/>
      </Box>
    );
  }
}

const mapStateToProps = state => ({
  wasAnError: state
    .rootReducer
    .stateReducer
    .signInReducer
    .statusOfSigninIn
    .wasAnError,
  errorMessage: state
    .rootReducer
    .stateReducer
    .signInReducer
    .statusOfSigninIn
    .error
    .errorMessage,
  authenticated: state
    .rootReducer
    .sessionReducer
    .authenticated
});

const mapDispatchToProps = dispatch => ({
  goHome: () => dispatch(replace(HOME)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login));
