/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Spinning from 'grommet/components/icons/Spinning';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { JsonForm } from 'component/';
import { SAVE_ROLE } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert, ToolBar } from 'component/';
import { ROLES } from 'config';
import { TXT_51, TXT_52, TXT_53, TXT_54, TXT_55, TXT_56, } from 'string';

class RegisterRoleForm extends Component {

  constructor(props) {
    super(props);

    this._onAddRole = this.onAddRole.bind(this);
    this._onBackButtonClicked= this.onBackButtonClicked.bind(this);

    this.state = {
      body: this.getBody(),
      footer: this.getFooter(),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    //To show a spinning
    if (nextProps.isSavingRole)
      prevState.footer.button.icon = <Spinning size={'xsmall'}/>;
    else
      delete prevState.footer.button.icon;
    //To show fieldErrorMessages
    if (nextProps.wasAnError && nextProps.isSavingRole)
      prevState.body.inputs.map((input, index) =>
        input.errors = nextProps.fieldErrorMessages[input.id]
      );
    //To delete fieldErrorMessages
    if (nextProps.wasASuccess && nextProps.isSavingRole)
      prevState.body.inputs.map((input, index) =>
        input.errors = []
      );

    return null;
  }

  render() {

    return (
      <Box
        flex
        id={'registerRoleForm'}>

        <ToolBar
          title={TXT_56}
          onBackButtonClicked={this._onBackButtonClicked}/>

        <Alert
          status={this.props.wasAnError ? 'critical' : 'ok'}
          visibility={this.props.wasAnError || this.props.wasASuccess}
          message={this.props.errorMessage || TXT_54}/>

        <Box
          flex
          justify={'center'}
          align={'center'}
          pad={'medium'}>
          <JsonForm
            ref={'form'}
            container={this.getContainer()}
            header={this.getHeader()}
            body={this.state.body}
            footer={this.state.footer}/>
        </Box>
      </Box>
    );
  }

  getContainer() {

    return {
      plain: true,
      onSubmit: this._onAddRole
    };
  }

  getBody() {

    return {
      inputs: [
        {
          id: 'name',
          type: 'textField',
          fieldContainer: {
            label: TXT_51,
          },
          field: {
          }
        },
        {
          id: 'description',
          type: 'textAreaField',
          fieldContainer: {
            label: TXT_52,
          },
          field: {
            rows: 5,
          }
        },
      ]
    };
  }

  getHeader() {

    return {
      title: TXT_55,
    };
  }

  getFooter() {

    return {
      container: {
        pad: { vertical: 'medium' },
      },
      button: {
        label: TXT_53,
        fill: true,
        type: 'submit',
        accent: true,
      },
    };
  }

  onAddRole(evt) {
    evt.preventDefault();
    this.props.saveRole(this.refs.form.getInputValues());
  }

  onBackButtonClicked() {
    this.props.goToRoles();
  }
}

const mapStateToProps = state => ({
  isSavingRole: state
    .rootReducer
    .stateReducer
    .saveRoleReducer
    .statusOfSavingRole
    .isSavingRole,
  wasAnError: state
    .rootReducer
    .stateReducer
    .saveRoleReducer
    .statusOfSavingRole
    .wasAnError,
  wasASuccess: state
    .rootReducer
    .stateReducer
    .saveRoleReducer
    .statusOfSavingRole
    .wasASuccess,
  errorMessage: state
    .rootReducer
    .stateReducer
    .saveRoleReducer
    .statusOfSavingRole
    .error
    .errorMessage,
  fieldErrorMessages: state
    .rootReducer
    .stateReducer
    .saveRoleReducer
    .statusOfSavingRole
    .error
    .fieldErrorMessages,
});

const mapDispatchToProps = dispatch => ({
  saveRole: (data) => dispatch(actionBuilder(SAVE_ROLE, data)),
  goToRoles: () => dispatch(replace(ROLES)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterRoleForm));
