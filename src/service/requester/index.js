export default class Requester {

  constructor(fetch) {
    this.fetch = fetch;
  }

  get(url) {
    return this.fetch.get(url)
    .then(this.errorHandler)
    .catch((error) => this.getError(error));
  }

  post(url, body) {
    return this.fetch.post(url, body)
    .then(this.errorHandler)
    .catch((error) => this.getError(error));
  }

  patch(url, body) {
    return this.fetch.patch(url, body)
    .then(this.errorHandler)
    .catch((error) => this.getError(error));
  }

  delete(url, body) {
    return this.fetch.delete(url, body)
    .then(this.errorHandler)
    .catch((error) => this.getError(error));
  }

  errorHandler(response) {
    switch (response.status) {

      case 200://Ok
        return response.data;

      case 400://Bad request
        return response.data;

      case 401://Unauthorized
        return response.data;

      default:
        return response.data;
    }
  }

  getError(error) {
    if (error.response && error.response.data)
      throw error.response.data.error;
    else
      throw new Error('Hubo un problema, lo siento :(');
  }
}
