/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import PropTypes from 'prop-types';
/****************
 * From project *
 ****************/
import { DataTable, FloatingButton } from './component/';
import { ToolBar } from 'component/';

class Crud extends Component {

  render() {

    return (
      <Box
        id={'crud'}
        full={true}>

        <ToolBar
          title={this.props.title}/>

        <DataTable
          ref={'dataTable'}
          header={this.props.header}
          data={this.props.data}
          emptyText={this.props.emptyText}
          fieldsToShow={this.props.fieldsToShow}
          onFetchMoreData={this.props.onFetchMoreData}
          onView={this.props.onView}
          onEdit={this.props.onEdit}
          onDelete={this.props.onDelete}
          fetchingData={this.props.fetchingData}/>

        <FloatingButton
          onAdd={this.props.onAdd}/>
      </Box>
    );
  }

  deleteItem(personId) {
    this.refs.dataTable.deleteItem(personId);
  }
}

Crud.propTypes = {
  title: PropTypes.string,
  header: PropTypes.array,
  data: PropTypes.array,
  emptyText: PropTypes.string,
  fieldsToShow: PropTypes.any,
  onFetchMoreData: PropTypes.func.isRequired,
  onView: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  fetchingData: PropTypes.bool,
  onAdd: PropTypes.func.isRequired,
};

export default Crud;
