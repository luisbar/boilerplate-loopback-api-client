/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import Button from 'grommet/components/Button';
import ViewIcon from 'grommet/components/icons/base/View';
import EditIcon from 'grommet/components/icons/base/Edit';
import TrashIcon from 'grommet/components/icons/base/Trash';
import Spinning from 'grommet/components/icons/Spinning';
import { Column, Table, AutoSizer, WindowScroller } from 'react-virtualized';
import 'react-virtualized/styles.css';
import PropTypes from 'prop-types';
/****************
 * From project *
 ****************/
import style from './style';

class DataTable extends Component {

  constructor(props) {
    super(props);

    this._onRenderAutoSizer = () => this.onRenderAutoSizer.bind(this);
    this._onRenderTable = height => this.onRenderTable.bind(this, height);
    this._noRowsRenderer = this.noRowsRenderer.bind(this);
    this._onRenderData = this.onRenderData.bind(this);
    this._onRenderColumn = this.onRenderColumn.bind(this);
    this._onCellRenderer = this.onCellRenderer.bind(this);
    this._onHeaderRenderer = this.onHeaderRenderer.bind(this);
    this._loadMoreRows = this.loadMoreRows.bind(this);
    this._onView = id => this.onView.bind(this, id);
    this._onEdit = id => this.onEdit.bind(this, id);
    this._onDelete = id => this.onDelete.bind(this, id);

    this.state = {
      data: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    if (nextProps.data && !!nextProps.data.length)
      return ({
        data: prevState.data.concat(nextProps.data),
      });

    return null;
  }

  render() {

    return (
      <Box
        flex
        style={style.container}>
        <WindowScroller>
          {this._onRenderAutoSizer()}
        </WindowScroller>
      </Box>
    );
  }

  onRenderAutoSizer({ height, isScrolling, onChildScroll, scrollTop }) {

    return (
      <AutoSizer>
        {this._onRenderTable(height)}
      </AutoSizer>
    );
  }

  onRenderTable(height, { width }) {

    return (
      <Box>
        <Table
          onScroll={this._loadMoreRows}
          noRowsRenderer={this._noRowsRenderer}
          width={width}
          height={height * 0.92}
          headerHeight={50}
          rowHeight={53}
          rowCount={this.state.data.length}
          rowGetter={this._onRenderData}>
          {!!this.state.data.length && ['Acciones'].concat(this.props.header).map(this._onRenderColumn)}
        </Table>
        {this.renderSpinner()}
      </Box>
    );
  }

  noRowsRenderer() {

    return (
      <Box
        flex
        align={'center'}>
        <Heading
          tag={'h3'}>
          {this.props.emptyText}
        </Heading>
      </Box>
    );
  }

  onRenderData({ index }) {

    return (this.state.data[index]);
  }

  onRenderColumn(label, index) {

    return (
      <Column
        key={index}
        label={label}
        flexShrink={index === 0 ? 0 : 1}
        cellRenderer={this._onCellRenderer}
        headerRenderer={this._onHeaderRenderer}
        dataKey={['action'].concat(this.props.fieldsToShow)[index]}
        width={index === 0 ? Math.max(document.documentElement.clientWidth, window.innerWidth || 0) / 2.5 : Math.max(document.documentElement.clientWidth, window.innerWidth || 0) / 4}/>
    );
  }

  onHeaderRenderer({ columnData, dataKey, disableSort, label, sortBy, sortDirection}) {
    if (dataKey === 'action')
      return this.renderColumnTitle(label, 'center');

    return this.renderColumnTitle(label);
  }

  renderColumnTitle(label, align) {

    return (
      <Box
        align={align}>
        <Heading
          truncate
          strong
          tag={'h5'}>
          {label}
        </Heading>
      </Box>
    );
  }

  onCellRenderer({cellData, columnData, columnIndex, dataKey, isScrolling, rowData, rowIndex}) {
    if (columnIndex === 0)
      return this.renderIcons(rowData.id);

    return cellData;
  }

  renderIcons(id) {

    return (
      <Box
        wrap
        responsive={false}
        direction={'row'}
        justify={'center'}>
        <Button
          icon={<ViewIcon colorIndex={'accent-3'}/>}
          onClick={this._onView(id)}/>
        <Button
          icon={<EditIcon colorIndex={'accent-3'}/>}
          onClick={this._onEdit(id)}/>
        <Button
          icon={<TrashIcon colorIndex={'accent-3'}/>}
          onClick={this._onDelete(id)}/>
      </Box>
    );
  }

  renderSpinner() {

    return (
      this.props.fetchingData
      ? <Spinning
          size={'medium'}
          style={style.spinner}/>
      : null
    );
  }

  loadMoreRows ({ clientHeight, scrollHeight, scrollTop }) {
    if (Math.round(clientHeight + scrollTop) === scrollHeight)
      this.props.onFetchMoreData();
  }

  onView(id) {
    this.props.onView(id);
  }

  onEdit(id) {
    this.props.onEdit(id);
  }

  onDelete(id) {
    this.props.onDelete(id);
  }

  deleteItem(personId) {
    this.setState({ data: this.state.data.filter((person) => person.id !== personId)})
  }
}

DataTable.propTypes = {
  header: PropTypes.array,
  data: PropTypes.array,
  emptyText: PropTypes.string,
  fieldsToShow: PropTypes.any,
  onFetchMoreData: PropTypes.func.isRequired,
  onView: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

DataTable.defaultProps = {
  header: ['Id', 'Nombre(s)', 'Apellido(s)', 'Correo'],
  emptyText: 'No hay datos',
};

export default DataTable;
