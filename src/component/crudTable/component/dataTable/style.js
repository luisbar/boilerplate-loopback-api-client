export default {

  container: {
    overflow: 'hidden',
    paddingLeft: '1%',
  },
  spinner: {
    position: 'absolute',
    top: '45%',
    left: '50%',
  },
};
