/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Pulse from 'grommet/components/icons/Pulse';
import PropTypes from 'prop-types';
/****************
 * From project *
 ****************/
import style from './style';

class FloatingButton extends Component {

  constructor(props) {
    super(props);

    this._onAdd = this.onAdd.bind(this);
  }

  render() {

    return (
      <Pulse
        style={style.pulse}
        onClick={this._onAdd}/>
    );
  }

  onAdd() {
    this.props.onAdd();
  }
}

FloatingButton.propTypes = {
  onAdd: PropTypes.func.isRequired,
};

export default FloatingButton;
