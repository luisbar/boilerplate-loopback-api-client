import DataTable from './dataTable/';
import FloatingButton from './floatingButton/';

export  {
  DataTable,
  FloatingButton,
};
