/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Form from 'grommet/components/Form';
import PropTypes from 'prop-types';
/****************
 * From project *
 ****************/
import { FormHeader, FormBody, FormFooter } from './component/';

class JsonForm extends Component {

  render() {
    const { container, header, body, footer } = this.props;

    return (
      <Form
        {...container}>
        <FormHeader
          {...header}/>
        <FormBody
          ref={'body'}
          {...body}/>
        <FormFooter
          {...footer}/>
      </Form>
    );
  }

  getInputValues() {

    return this.refs.body.getInputValues();
  }
}

JsonForm.propTypes = {
  container: PropTypes.object,
  header: PropTypes.object,
  body: PropTypes.object,
  Footer: PropTypes.object,
};

export default JsonForm;
