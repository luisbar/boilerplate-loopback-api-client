/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Footer from 'grommet/components/Footer';
import Button from 'grommet/components/Button';
import PropTypes from 'prop-types';

class FormFooter extends Component {

  render() {
    const { container, button } = this.props;

    return (
      <Footer
        {...container}>
        { !!button
          ? <Button
              {...button}/>
          : null
        }
      </Footer>
    );
  }
}

FormFooter.propTypes = {
  container: PropTypes.object,
  button: PropTypes.object,
};

export default FormFooter;
