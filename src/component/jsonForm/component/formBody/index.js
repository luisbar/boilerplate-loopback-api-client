/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Box from 'grommet/components/Box';
/****************
 * From project *
 ****************/
import { TextField, PasswordField, TextAreaField } from './component/';
/*************
 * Constants *
 *************/
const TEXT_FIELD = 'textField';
const PASSWORD_FIELD = 'passwordField';
const TEXT_AREA_FIELD = 'textAreaField';

class FormBody extends Component {

  constructor(props) {
    super(props);

    this._renderInputs = this.renderInputs.bind(this);
    this._onTextChange = this.onTextChange.bind(this);

    this.inputValues = {};
    this.props.inputs.map((input) => this.inputValues[input.id] = input.field.defaultValue)

    this.state = {
      inputValues: this.inputValues,
    };
  }

  render() {

    return (
      <Box>
        {this.props.inputs.map(this._renderInputs)}
      </Box>
    );
  }

  renderInputs(input, index) {

    switch (input.type) {

      case TEXT_FIELD:
        return <TextField
                key={index}
                id={input.id}
                fieldContainer={input.fieldContainer}
                field={input.field}
                onTextChange={this._onTextChange}
                errors={input.errors}/>;

      case PASSWORD_FIELD:
        return <PasswordField
                key={index}
                id={input.id}
                fieldContainer={input.fieldContainer}
                field={input.field}
                onTextChange={this._onTextChange}
                errors={input.errors}/>;

      case TEXT_AREA_FIELD:
        return <TextAreaField
                key={index}
                id={input.id}
                fieldContainer={input.fieldContainer}
                field={input.field}
                onTextChange={this._onTextChange}
                errors={input.errors}/>;

      default:
        return null;
    }
  }

  onTextChange(inputId, value) {
    const { inputValues } = this.state;
    inputValues[inputId] = value;
  }

  getInputValues() {

    return this.state.inputValues;
  }
}

FormBody.propTypes = {
  inputs: PropTypes.array,
};

FormBody.defaultProps = {
  inputs: [],
};

export default FormBody;
