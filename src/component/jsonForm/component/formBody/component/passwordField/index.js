/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import FormField from 'grommet/components/FormField';
import PasswordInput from 'grommet/components/PasswordInput';
/****************
 * From project *
 ****************/
import style from './style';

class PasswordField extends Component {

  constructor(props) {
    super(props);

    this._onTextChange = this.onTextChange.bind(this);
    this._renderErrors = this.renderErrors.bind(this);
  }

  render() {
    const { fieldContainer, field, errors } = this.props;

    return (
      <FormField
        {...fieldContainer}>
        <PasswordInput
          {...field}
          onChange={this._onTextChange}/>
        <Box
          pad={{ horizontal: 'medium' }}>
          {errors.map(this._renderErrors)}
        </Box>
      </FormField>
    );
  }

  onTextChange(evt) {
    this.props.onTextChange(this.props.id, evt.target.value);
  }

  renderErrors(error, index) {

    return (
      <Heading
        key={index}
        tag={'h6'}
        style={style.error}>
        {error}
      </Heading>
    );
  }
}

PasswordField.propTypes = {
  id: PropTypes.string,
  fieldContainer: PropTypes.object,
  field: PropTypes.object,
  onTextChange: PropTypes.func.isRequired,
  errors: PropTypes.array,
};

PasswordField.defaultProps = {
  errors: []
};

export default PasswordField;
