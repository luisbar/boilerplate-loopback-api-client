export default {

  textArea: {
    resize: 'none',
  },
  error: {
    color: '#F04953',
  },
};
