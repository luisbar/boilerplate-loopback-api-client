/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import PropTypes from 'prop-types';

class FormHeader extends Component {

  render() {

    const { container, titleContainer, title } = this.props;

    return (
      <Header
        {...container}>
        <Heading
          {...titleContainer}>
          {title}
        </Heading>
      </Header>
    );
  }
}

FormHeader.propTypes = {
  container: PropTypes.object,
  titleContainer: PropTypes.object,
  title: PropTypes.string,
};

export default FormHeader;
