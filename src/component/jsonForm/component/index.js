import FormFooter from './formFooter/';
import FormHeader from './formHeader/';
import FormBody from './formBody/';

export {
  FormFooter,
  FormHeader,
  FormBody,
};
