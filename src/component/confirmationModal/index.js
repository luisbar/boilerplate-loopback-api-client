/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Layer from 'grommet/components/Layer';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Button from 'grommet/components/Button';
import Status from 'grommet/components/icons/Status';
import PropTypes from 'prop-types';

class ConfirmationModal extends Component {

  render() {
    const { title, description, cancelButtonText, acceptButtonText, onCancelButtonClicked, onAcceptButtonClicked, ...others } = this.props;

    return (
      !others.hidden
      ? <Layer
          {...others}>
          <Box>

            <Heading
              align={'center'}>
              {title}
            </Heading>

            <Paragraph>
              {description}
            </Paragraph>

            <Box
              direction={'row'}
              justify={'around'}
              pad={{ vertical: 'medium' }}>

              <Button
                fill
                critical
                label={cancelButtonText}
                icon={<Status value={'critical'}/>}
                onClick={onCancelButtonClicked}/>

              <Button
                fill
                secondary
                label={acceptButtonText}
                icon={<Status value={'ok'}/>}
                onClick={onAcceptButtonClicked}/>
            </Box>
          </Box>
        </Layer>
      : null
    );
  }
}

ConfirmationModal.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  cancelButtonText: PropTypes.string,
  acceptButtonText: PropTypes.string,
  onCancelButtonClicked: PropTypes.func.isRequired,
  onAcceptButtonClicked: PropTypes.func.isRequired,
};

ConfirmationModal.defaultProps = {
  title: 'Title',
  description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae purus arcu. Nullam iaculis massa sit amet mauris tincidunt vestibulum. Sed vitae pharetra diam, sed facilisis velit. Mauris placerat tincidunt ullamcorper. Suspendisse tempor felis sapien, sed ornare nisi cursus vitae. Maecenas nec eros ullamcorper, pharetra diam ac, lobortis augue. Nullam eget vehicula velit, gravida accumsan libero. Duis at consequat est. Integer hendrerit metus vel risus ultricies, auctor consectetur ante feugiat. Suspendisse potenti. Duis maximus vehicula lorem, in tincidunt sem hendrerit non. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

Etiam eu sem sed magna suscipit vehicula non eu lorem. Duis condimentum, urna quis lobortis bibendum, nibh dui suscipit tortor, vel eleifend tellus metus ut est. Proin pellentesque arcu ac nunc dapibus, vitae elementum ipsum dignissim. Praesent viverra commodo purus et sodales. Morbi interdum justo nulla, eget elementum dolor congue sit amet. Nullam malesuada facilisis finibus. Maecenas odio lectus, vulputate eu dolor in, viverra viverra mauris. Sed non elit quis dui dictum molestie at ut ex. Vestibulum et dui auctor, consequat libero fringilla, imperdiet lorem. Ut tincidunt gravida tortor eu convallis.`,
  cancelButtonText: 'Cancel',
  acceptButtonText: 'Accept',
};

export default ConfirmationModal;
