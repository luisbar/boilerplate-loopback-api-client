/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import Logout from 'grommet/components/icons/base/Logout';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux'
/****************
 * From project *
 ****************/
import { TXT_12 } from 'string';
import { LOGIN } from 'config';
import { SIGN_OUT } from 'common/actions';
import actionBuilder from 'common/actionBuilder';
import { Alert } from 'component/';

class DrawerFooter extends Component {

  constructor(props) {
    super(props);

    this._onSignOutClicked =  this.onSignOutClicked.bind(this);

    this.state = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.checked && !nextProps.authenticated)
      nextProps.goLogin();

    return null;
  }

  render() {

    return (
      <Box
        direction={'row'}
        align={'center'}
        responsive={false}
        onClick={this._onSignOutClicked}>

        <Alert
          status={'critical'}
          visibility={this.props.wasAnError}
          message={this.props.errorMessage}/>

        <Box
          pad={'medium'}>
          <Logout/>
        </Box>

        <Heading
          truncate
          margin={'none'}
          tag={'h3'}>
          {TXT_12}
        </Heading>
      </Box>
    );
  }

  onSignOutClicked() {
    this.props.signOut();
  }
}

const mapStateToProps = state => ({
  wasAnError: state
    .rootReducer
    .stateReducer
    .signOutReducer
    .statusOfSigninOut
    .wasAnError,
  errorMessage: state
    .rootReducer
    .stateReducer
    .signOutReducer
    .statusOfSigninOut
    .error
    .errorMessage,
  checked: state
    .rootReducer
    .sessionReducer
    .checked,
  authenticated: state
    .rootReducer
    .sessionReducer
    .authenticated
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(actionBuilder(SIGN_OUT)),
  goLogin: () => dispatch(replace(LOGIN)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerFooter));
