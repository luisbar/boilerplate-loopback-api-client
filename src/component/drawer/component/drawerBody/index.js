/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { replace } from 'react-router-redux';
/****************
 * From project *
 ****************/
import { TXT_9, TXT_10, TXT_11 } from 'string';
import { HOME, ROLES, ROLES_ALLOCATION } from 'config';

class DrawerBody extends Component {

  render() {

    return (
      <Box
        flex
        justify={'center'}
        pad={{ horizontal: 'medium' }}>

        <Box
          onClick={this.props.goToHome}>
          <Heading
            tag={'h3'}
            strong={this.props.pathname === HOME}>
            {TXT_11}
          </Heading>
        </Box>

        <Box
          onClick={this.props.goToRoles}>
          <Heading
            tag={'h3'}
            strong={this.props.pathname === ROLES}>
            {TXT_9}
          </Heading>
        </Box>

        <Box
          onClick={this.props.goToRolesAllocation}>
          <Heading
            tag={'h3'}
            strong={this.props.pathname === ROLES_ALLOCATION}>
            {TXT_10}
          </Heading>
        </Box>
      </Box>
    );
  }
}

DrawerBody.propTypes = {
  pathname: PropTypes.string,
};

const mapDispatchToProps = dispatch => ({
  goToHome: () => dispatch(replace(HOME)),
  goToRoles: () => dispatch(replace(ROLES)),
  goToRolesAllocation: () => dispatch(replace(ROLES_ALLOCATION)),
});

export default withRouter(connect(
  null,
  mapDispatchToProps
)(DrawerBody));
