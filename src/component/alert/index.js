/********************
 * From third party *
 ********************/
import React, { Component } from 'react';
import Toast from 'grommet/components/Toast';
import PropTypes from 'prop-types';

class Alert extends Component {

  constructor(props) {
    super(props);

    this.state = {
      visibility: false,
      message: null,
      status: null,
    };

    this._onToastClosed = this.onToastClosed.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.visibility && !prevState.visibility)
      return { visibility: true, message: nextProps.message, status: nextProps.status }

    return null;
  }

  render() {
    const { visibility, message, status, ...others } = this.props;

    return (
      this.state.visibility
      ? <Toast
          {...others}
          status={this.state.status}
          onClose={this._onToastClosed}>
          {this.state.message}
        </Toast>
      : null
    );
  }

  onToastClosed() {
    this.setState({ visibility: false, message: null });
  }
}

Alert.propTypes = {
  message: PropTypes.string,
  visibility: PropTypes.bool.isRequired,
};

Alert.defaultProps = {
  duration: 3000,
};

export default Alert;
