//View ids
const LOGIN = '/login';
const HOME = '/';
const REGISTER_PERSON_FORM = '/person/add';
const PERSON_VIEWER = '/person/:id';
const UPDATE_PERSON_FORM = '/person/update/:id';
const ROLES = '/roles';
const ROLES_ALLOCATION = '/roles/allocation';
const REGISTER_ROLE_FORM = '/role/add';
const ROLE_VIEWER = '/role/:id';
const UPDATE_ROLE_FORM = '/role/update/:id';

//Endpoints
const URL = 'http://localhost:3002/api/';
const VERSION = 'v1.0.0';
const URL_PLUS_VERSION = `${URL}${VERSION}`;
const SIGN_IN_URI = '/persons/login/';
const SIGN_OUT_URI = '/persons/logout/?access_token=tk';
const FETCH_PERSONS_BY_PAGE_URI = '/persons/?filter[skip]=start&filter[limit]=end&access_token=tk';
const SAVE_PERSON_URI = '/persons?access_token=tk';
const FETCH_PERSON_URI = 'persons/id?access_token=tk';
const UPDATE_PERSON_URI = '/persons?access_token=tk';
const DELETE_PERSON_URI = '/persons/id?access_token=tk';
const FETCH_ROLES_BY_PAGE_URI = '/roles/?filter[skip]=start&filter[limit]=end&access_token=tk';
const SAVE_ROLE_URI = '/roles?access_token=tk';
const FETCH_ROLE_URI = 'roles/id?access_token=tk';
const UPDATE_ROLE_URI = '/roles?access_token=tk';
const DELETE_ROLE_URI = '/roles/id?access_token=tk';

export {
  LOGIN,
  HOME,
  REGISTER_PERSON_FORM,
  PERSON_VIEWER,
  UPDATE_PERSON_FORM,
  ROLES,
  REGISTER_ROLE_FORM,
  ROLE_VIEWER,
  UPDATE_ROLE_FORM,
  ROLES_ALLOCATION,
  URL_PLUS_VERSION,
  SIGN_IN_URI,
  SIGN_OUT_URI,
  FETCH_PERSONS_BY_PAGE_URI,
  SAVE_PERSON_URI,
  FETCH_PERSON_URI,
  UPDATE_PERSON_URI,
  DELETE_PERSON_URI,
  FETCH_ROLES_BY_PAGE_URI,
  SAVE_ROLE_URI,
  FETCH_ROLE_URI,
  UPDATE_ROLE_URI,
  DELETE_ROLE_URI,
};
